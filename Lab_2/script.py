import json


class AdjacencyMatrix:

    def __init__(self, file):
        self.adj_matrix = json.load(file)
        self.vertices_count = len(self.adj_matrix[0])

    def get_vertex_degree(self, vertex_index):
        count = 0

        for connection in self.get_vertex(vertex_index):
            if connection:
                count += 1

        return count

    def get_vertex(self, vertex_index):
        return self.adj_matrix[vertex_index]

    def iter_vertices(self):
        return range(self.vertices_count)

    def get_adj_vertices(self, vertex_index):
        for index, connection in enumerate(self.get_vertex(vertex_index)):
            if connection:
                yield index

    def get_euler_graph_type(self):
        odd_deg_vertices = 0

        for vertex in self.iter_vertices():
            if self.get_vertex_degree(vertex) % 2:
                odd_deg_vertices += 1

        if odd_deg_vertices == 0:
            return "Has euler circuit"
        elif odd_deg_vertices == 2:
            return "Has euler path"

        return "Isn't euler graph"

    def delete_edge(self, first_vertex, second_vertex):
        self.get_vertex(first_vertex)[second_vertex] = 0
        self.get_vertex(second_vertex)[first_vertex] = 0


with open("graph.json") as f:
    adj_matr = AdjacencyMatrix(f)


def find_start_vertex():
    odd_deg_vertices = 0
    odd_graph = None

    for vertex in adj_matr.iter_vertices():
        if adj_matr.get_vertex_degree(vertex) % 2:
            odd_deg_vertices += 1
            odd_graph = vertex

    if odd_deg_vertices == 0:
        return 0
    elif odd_deg_vertices == 2:
        return odd_graph

    return None


def advance_edge(current_vertex, visited_vertices, circuit):
    for neighbor in adj_matr.get_adj_vertices(current_vertex):
        visited_vertices.append(neighbor)
        adj_matr.delete_edge(current_vertex, neighbor)
        advance_edge(neighbor, visited_vertices, circuit)

    if visited_vertices:
        visited_vertices.pop()
        circuit.append(current_vertex)


def print_path(circuit):
    for index, vertex in enumerate(circuit):
        print(str(vertex+1), end="")

        if not index == len(circuit) - 1:
            print("->", end="")


def process():
    start_vertex = find_start_vertex()
    if start_vertex is None:
        print("The given graph isn't an euler")
        return

    visited_vertices = [start_vertex]
    circuit = []
    advance_edge(start_vertex, visited_vertices, circuit)

    if (circuit[0] == circuit[-1]):
        print("Euler circuit:")
    else:
        print("Euler path:")
    print_path(circuit)


if __name__ == "__main__":
    process()
