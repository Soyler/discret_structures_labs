import json

tree = None
with open("bin_tree.json", "r") as f:
    tree = json.load(f)


def backward_pass(node):
    if node is None:
        return

    left_child, right_child = get_children(tree, node)
    backward_pass(left_child)
    backward_pass(right_child)

    print("->" + str(node), end="")


def forward_pass(node):
    if node is None:
        return

    print("->" + str(node), end="")
    left_child, right_child = get_children(tree, node)
    forward_pass(left_child)
    forward_pass(right_child)


def backward_pass(node):
    if node is None:
        return

    left_child, right_child = get_children(tree, node)
    backward_pass(left_child)
    backward_pass(right_child)

    print("->" + str(node), end="")


def inner_pass(node):
    if node is None:
        return

    left_child, right_child = get_children(tree, node)
    inner_pass(left_child)
    print("->" + str(node), end="")
    inner_pass(right_child)


def get_children(tree, node):
    left_pos, left_child = get_child_node(tree, node)
    _, right_child = get_child_node(tree, node, left_pos+1)

    return left_child, right_child


def get_parent_node(tree, node, start=0):
    if start > len(tree) - 1:
        return None

    for index, edge in enumerate(tree[start:]):
        if edge[1] == node:
            return (index, edge[0])

    return len(tree), None


def get_child_node(tree, node, start=0):
    for index, edge in enumerate(tree[start:]):
        if edge[0] == node:
            return index, edge[1]

    return len(tree), None


root = tree[0][0]
print("Forward pass:")
forward_pass(root)
print()
print("Inner pass:")
inner_pass(root)
print()
print("Backward pass:")
backward_pass(root)
print()
