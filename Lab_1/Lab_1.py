import json


def compare_edges(first_edge, second_edge):
    if first_edge[0] == second_edge[0] and first_edge[1] == second_edge[1] \
            or first_edge[0] == second_edge[1] and first_edge[1] == second_edge[0]:
        return True

    return False


def iterate_edges(first_graph, second_graph):
    for first_graph_edge in first_graph:

        indexes_to_delete = []
        for index, second_graph_edge in enumerate(second_graph):
            if compare_edges(first_graph_edge, second_graph_edge):
                indexes_to_delete.append(index)

        for index in reversed(indexes_to_delete):
            del second_graph[index]

    if not second_graph:
        return True

    return False


first_graph_edges = None
second_graph_edges = None
isometric = False

with open("data.json", "r") as f:
    first_graph_edges, second_graph_edges = json.load(f)

is_second_isomorphic = iterate_edges(list(first_graph_edges), list(second_graph_edges))
is_first_isomorphic = iterate_edges(list(second_graph_edges), list(first_graph_edges))

if is_first_isomorphic and is_second_isomorphic:
    print("Isomorphic graphs")
else:
    print("Not isometric graphs")
